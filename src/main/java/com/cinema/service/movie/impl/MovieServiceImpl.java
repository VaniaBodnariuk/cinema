package com.cinema.service.movie.impl;

import com.cinema.model.Genre;
import com.cinema.model.Movie;
import com.cinema.repository.movie.MovieRepository;
import com.cinema.service.movie.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {
    private final MovieRepository repository;

    @Override
    public void create(Movie model) {
        log.info("Entering method create with Movie model: {}",
                 model);
        repository.create(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Movie> getAll() {
        log.info("Entering method getAll");
        List<Movie> result = repository.getAll();
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Movie getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        Movie result =  repository.getById(id);
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Movie model) {
        log.info("Entering method update with Movie model: {}",
                 model);
        repository.update(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        repository.deleteById(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        repository.synchronize();
        log.info("Exiting method synchronize");
    }

    @Override
    public Set<Genre> getGenresByMovieId(UUID movieId) {
        log.info("Entering method getGenresByMovieId "
                + "with UUID movieId {}", movieId);
        Set<Genre> result =  repository.getGenresByMovieId(movieId);
        log.info("Exiting method getGenresByMovieId with result {}", result);
        return result;
    }
}
