package com.cinema.service.genre.impl;

import com.cinema.model.Genre;
import com.cinema.repository.genre.GenreRepository;
import com.cinema.service.genre.GenreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {
    private final GenreRepository repository;

    @Override
    public void create(Genre model) {
        log.info("Entering method create with Genre model: {}",
                 model);
        repository.create(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Genre> getAll() {
        log.info("Entering method getAll");
        List<Genre> result = repository.getAll();
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Genre getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        Genre result = repository.getById(id);
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Genre model) {
        log.info("Entering method update with Genre model: {}",
                 model);
        repository.update(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        repository.deleteById(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        repository.synchronize();
        log.info("Exiting method synchronize");
    }
}
