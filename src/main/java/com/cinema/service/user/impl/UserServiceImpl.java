package com.cinema.service.user.impl;

import com.cinema.model.Ticket;
import com.cinema.model.User;
import com.cinema.repository.user.UserRepository;
import com.cinema.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    @Override
    public void create(User model) {
        log.info("Entering method create with User model: {}",
                model);
        repository.create(model);
        log.info("Exiting method create");
    }

    @Override
    public List<User> getAll() {
        log.info("Entering method getAll");
        List<User> result = repository.getAll();
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public User getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        User result =  repository.getById(id);
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(User model) {
        log.info("Entering method update with User model: {}",
                model);
        repository.update(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        repository.deleteById(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        repository.synchronize();
        log.info("Exiting method synchronize");
    }
}
