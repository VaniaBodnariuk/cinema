package com.cinema.service.ticket.impl;

import com.cinema.model.Movie;
import com.cinema.model.Ticket;
import com.cinema.model.User;
import com.cinema.repository.ticket.TicketRepository;
import com.cinema.service.ticket.TicketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.*;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {
    private final TicketRepository repository;

    @Override
    public void create(Ticket model) {
        log.info("Entering method create with Ticket model: {}",
                 model);
        repository.create(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Ticket> getAll() {
        log.info("Entering method getAll");
        List<Ticket> result = repository.getAll();
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Ticket getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        Ticket result =  repository.getById(id);
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Ticket model) {
        log.info("Entering method update with Ticket model: {}",
                 model);
        repository.update(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        repository.deleteById(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public List<Movie> getMoviesByTicketForToday() {
        log.info("Entering method getMoviesByTicketForToday");
        List<Movie> result = repository.getMoviesByTicketForToday();
        log.info("Exiting method getMoviesByTicketForToday with result {}",
                 result);
        return result;
    }

    @Override
    public List<User> getUsersByMovieAndDate(Movie movie, LocalDate showDate) {
        log.info("Entering method getUsersByMovieAndDate " +
                 "with Movie movie = {}; LocalDate showDate= {}",
                 movie, showDate);
        List<User> result = repository.getUsersByMovieAndDate(movie, showDate);
        log.info("Exiting method getUsersByMovieAndDate with result {}",
                 result);
        return result;
    }

    @Override
    public double getIncomeForMonth(int month, int year) {
        log.info("Entering method getIncomeForMonth " +
                 "with int month = {}; int year= {}",
                 month, year);
        double result = repository.getIncomeForMonth(month, year);
        log.info("Exiting method getIncomeForMonth with result {}",
                 result);
        return result;
    }

    @Override
    public Map<Movie, Long> getMovieRatingByTicketsAmount() {
        log.info("Entering method getMovieRatingByTicketsAmount");
        Map<Movie, Long> result = repository.getMovieRatingByTicketsAmount();
        log.info("Exiting method getMovieRatingByTicketsAmount with result {}",
                 result);
        return result;
    }

    @Override
    public Map<Movie, Long> getMovieRatingByTicketsAmountInAscThatLess(
                                                     long ticketsAmount) {
        log.info("Entering method getMovieRatingByTicketsAmountInAscThatLess " +
                 "with long ticketsAmount = {}", ticketsAmount);
        Map<Movie, Long> result
                = repository.getMovieRatingByTicketsAmountInAscThatLess(
                                                           ticketsAmount);
        log.info("Exiting method getMovieRatingByTicketsAmountInAscThatLess" +
                " with result {}", result);
        return result;
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        repository.synchronize();
        log.info("Exiting method synchronize");
    }
}