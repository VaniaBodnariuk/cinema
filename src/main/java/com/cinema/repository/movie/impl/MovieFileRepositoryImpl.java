package com.cinema.repository.movie.impl;

import com.cinema.exception.NotFoundException;
import com.cinema.exception.UniqueFieldException;
import com.cinema.model.Genre;
import com.cinema.model.Movie;
import com.cinema.model.Ticket;
import com.cinema.repository.movie.MovieRepository;
import com.cinema.repository.ticket.TicketRepository;
import com.cinema.utility.file.basic.FileUtility;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.util.*;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

@Slf4j
public class MovieFileRepositoryImpl implements MovieRepository {
    private final FileUtility<Movie> fileUtility;
    private final Map<UUID, Movie> localStorage;
    private final TicketRepository ticketRepository;

    public MovieFileRepositoryImpl(FileUtility<Movie> fileUtility,
                                   TicketRepository ticketRepository) {
        this.fileUtility = fileUtility;
        this.localStorage = getDataFromFileViaMap();
        this.ticketRepository = ticketRepository;
    }

    @Override
    public void create(Movie model) {
        log.info("Entering method create with Movie model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        checkTitleAndProducerNameForUniqueness(model);
        save(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Movie> getAll() {
        log.info("Entering method getAll");
        List<Movie> result = localStorage.values()
                .stream()
                .map(Movie::createCopy)
                .collect(toList());
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Movie getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        checkIdForExisting(id);
        Movie result = localStorage.get(id).createCopy();
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Movie model) {
        log.info("Entering method update with Movie model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        Movie oldModel = getById(model.getId());
        if(!oldModel.equals(model)) {
            checkTitleAndProducerNameForUniqueness(model);
        }
        updateReferencesInTickets(model);
        save(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id){
        log.info("Entering method deleteById with UUID id: {}", id);
        checkIdForExisting(id);
        deleteRelatedTickets(getById(id));
        localStorage.remove(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        fileUtility.write(new ArrayList<>(localStorage.values()));
        log.info("Exiting method synchronize");
    }

    @Override
    public Set<Genre> getGenresByMovieId(UUID movieId) {
        log.info("Entering method getGenresByMovieId "
                 + "with UUID movieId {}", movieId);
        Set<Genre> result =  getById(movieId).getGenres();
        log.info("Exiting method getGenresByMovieId with result {}", result);
        return result;
    }

    private void save(Movie model){
        localStorage.put(model.getId(), model);
    }

    private void checkIdForExisting(UUID id) {
        if(!localStorage.containsKey(id)) {
            NotFoundException exception
                    = new NotFoundException(Movie.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private Map<UUID, Movie> getDataFromFileViaMap() {
        List<Movie> dataList = fileUtility.read();
        return convertDataListToDataMap(dataList);
    }

    private Map<UUID, Movie> convertDataListToDataMap(List<Movie> dataList) {
        Map<UUID,Movie> dataMap = new HashMap<>();
        dataList.forEach(movie -> dataMap.put(movie.getId(), movie));
        return dataMap;
    }

    private void checkTitleAndProducerNameForUniqueness(Movie model) {
        if(localStorage.containsValue(model)){
            UniqueFieldException exception
                    = new UniqueFieldException(model.getClass().getName(),
                    model.getId(),"title and producerName");
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private Stream<Ticket> findReferencesInTickets(Movie model) {
        return ticketRepository.getAll()
                .stream()
                .filter(ticket -> ticket.getMovie().equals(model));
    }

    private void updateReferencesInTickets(Movie model) {
        findReferencesInTickets(model).forEach(ticket ->
                updateReferenceInTicket(ticket,model)
        );
    }

    private void updateReferenceInTicket(Ticket ticket, Movie movie) {
        ticket.setMovie(movie);
        ticketRepository.update(ticket);
    }

    private void deleteRelatedTickets(Movie model) {
        findReferencesInTickets(model).forEach(ticket ->
                ticketRepository.deleteById(ticket.getId()));
    }
}
