package com.cinema.repository.movie.impl;

import com.cinema.configuration.JdbcConfiguration;
import com.cinema.exception.DataException;
import com.cinema.exception.NotFoundException;
import com.cinema.exception.UniqueFieldException;
import com.cinema.mapper.GenreMapper;
import com.cinema.mapper.MovieMapper;
import com.cinema.model.Genre;
import com.cinema.model.Movie;
import com.cinema.repository.movie.MovieRepository;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.sql.*;
import java.util.*;

@Slf4j
public class MovieJdbcRepositoryImpl implements MovieRepository {
    private final GenreMapper genreMapper;
    private final MovieMapper movieMapper;
    private final JdbcConfiguration configuration;

    private final String SQL_SELECT_GENRES_BY_MOVIE_ID
            = "SELECT genre.* "
            + "FROM public.movie_has_genre "
            + "JOIN genre on genre.id = movie_has_genre.genre_id "
            + "WHERE movie_id = ?";

    private final String SQL_SELECT_ALL = "SELECT * FROM public.movie";

    private final String SQL_SELECT_BY_ID
            = "SELECT * "
            + "FROM public.movie "
            + "WHERE id = ?";

    private final String SQL_INSERT
            = "INSERT INTO public.movie (id, title, producer_name, rating, duration) "
            + "VALUES (?, ?, ?, ?, ?)";

    private final String SQL_UPDATE
            = "UPDATE public.movie "
            + "SET title = ?, producer_name = ?, rating = ?, duration = ? "
            + "WHERE id = ?";

    private final String SQL_DELETE_BY_ID = "DELETE FROM public.movie WHERE id = ?";

    private final String SQL_SELECT_BY_TITLE_AND_PRODUCER_NAME
            = "SELECT * "
            + "FROM public.movie "
            + "WHERE title = ? AND producer_name = ?";

    public MovieJdbcRepositoryImpl(GenreMapper genreMapper, MovieMapper movieMapper) {
        this.genreMapper = genreMapper;
        this.movieMapper = movieMapper;
        this.configuration = com.cinema.configuration.JdbcConfiguration.createInstanceForMain();
    }


    @Override
    public List<Movie> getAll() {
        log.info("Entering method getAll");
        try (Connection connection = configuration.getConnection();
             Statement statement
                     = connection.createStatement();
             ResultSet rs = statement.executeQuery(SQL_SELECT_ALL)) {
            List<Movie> models = new ArrayList<>();
            while (rs.next()) {
                models.add(movieMapper.mapJdbcResultToModel(rs));
            }
            log.info("Exiting method getAll with result {}", models);
            return models;
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public Movie getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            preparedStatement.setObject(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                checkIdForExisting(rs.next(), id);
                Movie movie = movieMapper.mapJdbcResultToModel(rs);
                log.info("Exiting method getById with result {}", movie);
                return movie;
            }
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void create(Movie model) {
        log.info("Entering method create with Movie model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        checkTitleAndProducerNameForUniqueness(model);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_INSERT)) {
            preparedStatement.setObject(1, model.getId());
            preparedStatement.setString(2, model.getTitle());
            preparedStatement.setString(3, model.getProducerName());
            preparedStatement.setDouble(4, model.getRating());
            preparedStatement.setString(5, model.getDuration().toString());
            preparedStatement.executeUpdate();
            log.info("Exiting method create");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void update(Movie model) {
        log.info("Entering method update with Movie model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        Movie oldModel = getById(model.getId());
        if(!oldModel.equals(model)){
            checkTitleAndProducerNameForUniqueness(model);
        }
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, model.getTitle());
            preparedStatement.setString(2, model.getProducerName());
            preparedStatement.setDouble(3, model.getRating());
            preparedStatement.setString(4, model.getDuration().toString());
            preparedStatement.setObject(5, model.getId());
            preparedStatement.executeUpdate();
            log.info("Exiting method update");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            preparedStatement.setObject(1, id);
            checkIdForExisting(preparedStatement.executeUpdate(), id);
            log.info("Exiting method deleteById");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void synchronize() {
        UnsupportedOperationException exception
                = new UnsupportedOperationException(
                "Method synchronize is not defined");
        log.error("An exception occurred!",exception);
        throw exception;
    }

    @Override
    public Set<Genre> getGenresByMovieId(UUID movieId) {
        log.info("Entering method getGenresByMovieId "
                 + "with UUID movieId {}", movieId);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(
                     SQL_SELECT_GENRES_BY_MOVIE_ID)) {
            preparedStatement.setObject(1, movieId);
            Set<Genre> genres = new HashSet<>();
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    genres.add(genreMapper.mapJdbcResultToModel(rs));
                }
            }
            log.info("Exiting method getGenresByMovieId with result {}", genres);
            return genres;
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }

    }

    private void checkIdForExisting(boolean isRsNext, UUID id) {
        if (!isRsNext) {
            NotFoundException exception
                    = new NotFoundException(Movie.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void checkIdForExisting(int executeUpdateResult, UUID id) {
        if (executeUpdateResult == 0) {
            NotFoundException exception
                    = new NotFoundException(Movie.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void checkTitleAndProducerNameForUniqueness(Movie model) {
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_TITLE_AND_PRODUCER_NAME)) {
            preparedStatement.setObject(1, model.getTitle());
            preparedStatement.setObject(2, model.getProducerName());
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    UniqueFieldException exception
                            = new UniqueFieldException(model.getClass().getName(),
                            model.getId(),"title and producerName");
                    log.error("An exception occurred!", exception);
                    throw exception;
                }
            }
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }
}
