package com.cinema.repository.user.impl;

import com.cinema.configuration.JdbcConfiguration;
import com.cinema.exception.DataException;
import com.cinema.exception.NotFoundException;
import com.cinema.exception.UniqueFieldException;
import com.cinema.mapper.UserMapper;
import com.cinema.model.User;
import com.cinema.repository.user.UserRepository;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
public class UserJdbcRepositoryImpl implements UserRepository {
    private final UserMapper userMapper;
    private final JdbcConfiguration configuration;

    private final String SQL_SELECT_ALL = "SELECT * FROM public.users";

    private final String SQL_SELECT_BY_ID
            = "SELECT * "
            + "FROM public.users "
            + "WHERE id = ?";

    private final String SQL_INSERT
            = "INSERT INTO public.users (id, name, phone) VALUES (?, ? , ?)";

    private final String SQL_UPDATE
            = "UPDATE public.users "
            + "SET name = ?, phone = ? "
            + "WHERE id = ?";

    private final String SQL_DELETE_BY_ID = "DELETE FROM public.users WHERE id = ?";

    private final String SQL_SELECT_BY_PHONE
            = "SELECT * "
            + "FROM public.users "
            + "WHERE phone = ?";

    public UserJdbcRepositoryImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
        this.configuration = JdbcConfiguration.createInstanceForMain();
    }

    @Override
    public List<User> getAll() {
        log.info("Entering method getAll");
        try (Connection connection = configuration.getConnection();
             Statement statement
                     = connection.createStatement();
             ResultSet rs = statement.executeQuery(SQL_SELECT_ALL)) {
            List<User> models = new ArrayList<>();
            while (rs.next()) {
                models.add(userMapper.mapJdbcResultToModel(rs));
            }
            log.info("Exiting method getAll with result {}", models);
            return models;
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public User getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            preparedStatement.setObject(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                checkIdForExisting(rs.next(), id);
                User result = userMapper.mapJdbcResultToModel(rs);
                log.info("Exiting method getById with result {}", result);
                return result;
            }
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void create(User model) {
        log.info("Entering method create with User model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        checkPhoneForUniqueness(model);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_INSERT)) {
            preparedStatement.setObject(1, model.getId());
            preparedStatement.setString(2, model.getName());
            preparedStatement.setString(3, model.getPhone());
            preparedStatement.executeUpdate();
            log.info("Exiting method create");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void update(User model) {
        log.info("Entering method update with User model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        User oldModel = getById(model.getId());
        if(!oldModel.getPhone().equals(model.getPhone())){
            checkPhoneForUniqueness(model);
        }
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getPhone());
            preparedStatement.setObject(3, model.getId());
            preparedStatement.executeUpdate();
            log.info("Exiting method update");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            preparedStatement.setObject(1, id);
            checkIdForExisting(preparedStatement.executeUpdate(), id);
            log.info("Exiting method deleteById");
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void synchronize() {
        UnsupportedOperationException exception
                = new UnsupportedOperationException(
                "Method synchronize is not defined");
        log.error("An exception occurred!",exception);
        throw exception;
    }

    private void checkIdForExisting(boolean isRsNext, UUID id) {
        if (!isRsNext) {
            NotFoundException exception
                    = new NotFoundException(User.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void checkIdForExisting(int executeUpdateResult, UUID id) {
        if (executeUpdateResult == 0) {
            NotFoundException exception
                    = new NotFoundException(User.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void checkPhoneForUniqueness(User model) {
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_PHONE)) {
            preparedStatement.setObject(1, model.getName());
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    UniqueFieldException exception
                            = new UniqueFieldException(model.getClass().getName(),
                                                       model.getId(),"phone");
                    log.error("An exception occurred!", exception);
                    throw exception;
                }
            }
        }catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }
}
