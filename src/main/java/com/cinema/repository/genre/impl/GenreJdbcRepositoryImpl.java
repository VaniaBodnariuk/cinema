package com.cinema.repository.genre.impl;

import com.cinema.configuration.JdbcConfiguration;
import com.cinema.exception.DataException;
import com.cinema.exception.NotFoundException;
import com.cinema.exception.UniqueFieldException;
import com.cinema.mapper.GenreMapper;
import com.cinema.model.Genre;
import com.cinema.repository.genre.GenreRepository;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
public class GenreJdbcRepositoryImpl implements GenreRepository {
    private final GenreMapper genreMapper;
    private final JdbcConfiguration configuration;

    private final String SQL_SELECT_ALL = "SELECT * FROM public.genre";

    private final String SQL_SELECT_BY_ID
            = "SELECT * "
            + "FROM public.genre "
            + "WHERE id = ?";

    private final String SQL_INSERT
            = "INSERT INTO public.genre (id, name, description) VALUES(?,?,?)";

    private final String SQL_UPDATE
            = "UPDATE public.genre "
            + "SET name = ?, description = ? "
            + "WHERE id = ?";

    private final String SQL_DELETE_BY_ID = "DELETE FROM public.genre WHERE id = ?";

    private final String SQL_SELECT_BY_NAME
            = "SELECT * "
            + "FROM public.genre "
            + "WHERE name = ?";

    public GenreJdbcRepositoryImpl(GenreMapper genreMapper) {
        this.genreMapper = genreMapper;
        this.configuration = JdbcConfiguration.createInstanceForMain();
    }


    @Override
    public List<Genre> getAll() {
        log.info("Entering method getAll");
        List<Genre> models = new ArrayList<>();
        try (Connection connection = configuration.getConnection();
             Statement statement
                     = connection.createStatement();
             ResultSet rs = statement.executeQuery(SQL_SELECT_ALL)) {
            while (rs.next()) {
                models.add(genreMapper.mapJdbcResultToModel(rs));
            }
            log.info("Exiting method getAll with result {}", models);
            return models;
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public Genre getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            preparedStatement.setObject(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                checkIdForExisting(rs.next(), id);
                Genre result = genreMapper.mapJdbcResultToModel(rs);
                log.info("Exiting method getById with result {}", result);
                return result;
            }
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void create(Genre model) {
        log.info("Entering method create with Genre model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        checkNameForUniqueness(model);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_INSERT)) {
            preparedStatement.setObject(1, model.getId());
            preparedStatement.setString(2, model.getName());
            preparedStatement.setString(3, model.getDescription());
            preparedStatement.executeUpdate();
            log.info("Exiting method create");
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void update(Genre model) {
        log.info("Entering method update with Genre model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        Genre oldModel = getById(model.getId());
        if(!oldModel.equals(model)) {
            checkNameForUniqueness(model);
        }
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getDescription());
            preparedStatement.setObject(3, model.getId());
            preparedStatement.executeUpdate();
            log.info("Exiting method update");
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            preparedStatement.setObject(1, id);
            checkIdForExisting(preparedStatement.executeUpdate(), id);
            log.info("Exiting method deleteById");
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }

    @Override
    public void synchronize() {
        UnsupportedOperationException exception
                = new UnsupportedOperationException(
                      "Method synchronize is not defined");
        log.error("An exception occurred!",exception);
        throw exception;
    }

    private void checkIdForExisting(boolean isRsNext, UUID id) {
        if (!isRsNext) {
            NotFoundException exception
                    = new NotFoundException(Genre.class.getName(), id);
            log.error("An exception occurred!",exception);
            throw exception;
        }
    }

    private void checkIdForExisting(int executeUpdateResult, UUID id) {
        if (executeUpdateResult == 0) {
            NotFoundException exception
                    = new NotFoundException(Genre.class.getName(), id);
            log.error("An exception occurred!",exception);
            throw exception;
        }
    }

    private void checkNameForUniqueness(Genre model) {
        try (Connection connection = configuration.getConnection();
             PreparedStatement preparedStatement
                     = connection.prepareStatement(SQL_SELECT_BY_NAME)) {
            preparedStatement.setObject(1, model.getName());
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    UniqueFieldException exception
                            = new UniqueFieldException(model.getClass().getName(),
                            model.getId(), "name");
                    log.error("An exception occurred!",exception);
                    throw exception;
                }
            }
        } catch (SQLException e) {
            DataException dataException
                    = new DataException(e.getMessage(), e.getCause());
            log.error("An exception occurred!", dataException);
            throw dataException;
        }
    }
}
