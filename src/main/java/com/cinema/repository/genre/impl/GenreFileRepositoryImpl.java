package com.cinema.repository.genre.impl;

import com.cinema.exception.UniqueFieldException;
import com.cinema.exception.NotFoundException;
import com.cinema.model.Genre;
import com.cinema.model.Movie;
import com.cinema.repository.genre.GenreRepository;
import com.cinema.repository.movie.MovieRepository;
import com.cinema.utility.file.basic.FileUtility;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.util.*;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

@Slf4j
public class GenreFileRepositoryImpl implements GenreRepository {
    private final FileUtility<Genre> fileUtility;
    private final Map<UUID, Genre> localStorage;
    private final MovieRepository movieRepository;

    public GenreFileRepositoryImpl(FileUtility<Genre> fileUtility,
                                   MovieRepository movieRepository) {
        this.fileUtility = fileUtility;
        this.localStorage = getDataFromFileViaMap();
        this.movieRepository = movieRepository;
    }

    @Override
    public void create(Genre model) {
        log.info("Entering method create with Genre model: {}", model);
        ValidatorUtility.validateModel(model);
        checkNameForUniqueness(model);
        save(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Genre> getAll() {
        log.info("Entering method getAll");
        List<Genre> result = localStorage.values()
                .stream()
                .map(Genre::createCopy)
                .collect(toList());
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Genre getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        checkIdForExisting(id);
        Genre result = localStorage.get(id).createCopy();
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Genre model) {
        log.info("Entering method update with Genre model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        Genre oldModel = getById(model.getId());
        if (!oldModel.equals(model)) {
            checkNameForUniqueness(model);
        }
        updateReferencesInMovies(model);
        save(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        checkIdForExisting(id);
        deleteReferencesInMovies(getById(id));
        localStorage.remove(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        fileUtility.write(new ArrayList<>(localStorage.values()));
        log.info("Exiting method synchronize");
    }

    private void save(Genre model) {
        localStorage.put(model.getId(), model);
    }

    private void checkNameForUniqueness(Genre model) {
        if (localStorage.containsValue(model)) {
            UniqueFieldException exception
                    = new UniqueFieldException(model.getClass().getName(),
                                               model.getId(), "name");
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void checkIdForExisting(UUID id) {
        if (!localStorage.containsKey(id)) {
            NotFoundException exception
                    = new NotFoundException(Genre.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private void updateReferencesInMovies(Genre model) {
        findReferencesInMovies(model).forEach(movie ->
                updateReferenceInMovie(movie, model));
    }

    private void updateReferenceInMovie(Movie movie, Genre genre) {
        movie.getGenres().add(genre);
        movieRepository.update(movie);
    }

    private void deleteReferenceInMovie(Movie movie, Genre genre) {
        movie.getGenres().remove(genre);
        movieRepository.update(movie);
    }

    private void deleteReferencesInMovies(Genre model) {
        findReferencesInMovies(model).forEach(movie ->
                deleteReferenceInMovie(movie, model));
    }

    private Map<UUID, Genre> getDataFromFileViaMap() {
        List<Genre> dataList = fileUtility.read();
        return convertDataListToDataMap(dataList);
    }

    private Stream<Movie> findReferencesInMovies(Genre model) {
        return movieRepository.getAll()
                .stream()
                .filter(movie -> isMovieHasGenre(movie, model));
    }

    private boolean isMovieHasGenre(Movie movie, Genre requiredGenre) {
        return movie.getGenres()
                .stream()
                .anyMatch(genre -> genre.getId()
                                        .equals(requiredGenre.getId()));
    }

    private Map<UUID, Genre> convertDataListToDataMap(List<Genre> dataList) {
        Map<UUID, Genre> dataMap = new HashMap<>();
        dataList.forEach(genre -> dataMap.put(genre.getId(), genre));
        return dataMap;
    }
}
