package com.cinema.repository.ticket.impl;

import com.cinema.exception.NotFoundException;
import com.cinema.model.Movie;
import com.cinema.model.Ticket;
import com.cinema.model.User;
import com.cinema.repository.ticket.TicketRepository;
import com.cinema.utility.file.basic.FileUtility;
import com.cinema.utility.validator.ValidatorUtility;
import lombok.extern.slf4j.Slf4j;
import java.time.LocalDate;
import java.util.*;
import static java.util.Comparator.reverseOrder;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.*;

@Slf4j
public class TicketFileRepositoryImpl implements TicketRepository {
    private final Map<UUID, Ticket> localStorage;
    private final FileUtility<Ticket> fileUtility;

    public TicketFileRepositoryImpl(FileUtility<Ticket> fileUtility) {
        this.fileUtility = fileUtility;
        this.localStorage = getDataFromFileViaMap();
    }

    @Override
    public void create(Ticket model) {
        log.info("Entering method create with Ticket model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        save(model);
        log.info("Exiting method create");
    }

    @Override
    public List<Ticket> getAll() {
        log.info("Entering method getAll");
        List<Ticket> result = localStorage.values()
                .stream()
                .map(Ticket::createCopy)
                .collect(toList());
        log.info("Exiting method getAll with result {}", result);
        return result;
    }

    @Override
    public Ticket getById(UUID id) {
        log.info("Entering method getById with UUID id: {}", id);
        checkIdForExisting(id);
        Ticket result = localStorage.get(id).createCopy();
        log.info("Exiting method getById with result {}", result);
        return result;
    }

    @Override
    public void update(Ticket model) {
        log.info("Entering method update with Ticket model: {}",
                 model);
        ValidatorUtility.validateModel(model);
        checkIdForExisting(model.getId());
        save(model);
        log.info("Exiting method update");
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Entering method deleteById with UUID id: {}", id);
        checkIdForExisting(id);
        localStorage.remove(id);
        log.info("Exiting method deleteById");
    }

    @Override
    public void synchronize() {
        log.info("Entering method synchronize");
        fileUtility.write(new ArrayList<>(localStorage.values()));
        log.info("Exiting method synchronize");
    }

    @Override
    public List<Movie> getMoviesByTicketForToday() {
        log.info("Entering method getMoviesByTicketForToday");
        List<Movie> result =  getAll().stream()
                .filter(this::isTicketForToday)
                .map(Ticket::getMovie)
                .distinct()
                .collect(toList());
        log.info("Exiting method getMoviesByTicketForToday with result {}",
                 result);
        return result;
    }

    @Override
    public List<User> getUsersByMovieAndDate(Movie movie,
                                             LocalDate showDate) {
        log.info("Entering method getUsersByMovieAndDate " +
                 "with Movie movie = {}; LocalDate showDate= {}",
                 movie, showDate);
        List<User> result =  getAll().stream()
                .filter(ticket -> isTicketForMovieAndDate(ticket,
                        movie,
                        showDate)
                )
                .map(Ticket::getUser)
                .collect(toList());
        log.info("Exiting method getUsersByMovieAndDate with result {}",
                 result);
        return result;
    }

    @Override
    public double getIncomeForMonth(int month, int year) {
        log.info("Entering method getIncomeForMonth " +
                 "with int month = {}; int year= {}",
                 month, year);
        double result = getAll().stream()
                .filter(ticket ->
                        isDateIncludedInMonth(
                                ticket.getDate()
                                      .toLocalDate(),
                                month, year)
                )
                .map(Ticket::getPrice)
                .reduce(0.0, Double::sum);
        log.info("Exiting method getIncomeForMonth with result {}",
                 result);
        return result;
    }

    @Override
    public Map<Movie, Long> getMovieRatingByTicketsAmount() {
        log.info("Entering method getMovieRatingByTicketsAmount");
        Map<Movie, Long> moviesWithTicketsAmount
                = getMoviesWithTicketsAmount();

        Map<Movie, Long> rating = new LinkedHashMap<>();
        moviesWithTicketsAmount.entrySet()
                .stream()
                .sorted(comparingByValue(reverseOrder()))
                .forEachOrdered(x -> rating.put(x.getKey(), x.getValue()));
        log.info("Exiting method getMovieRatingByTicketsAmount with result {}",
                 rating);
        return rating;
    }

    @Override
    public Map<Movie, Long> getMovieRatingByTicketsAmountInAscThatLess(
                                                     long ticketsAmount) {
        log.info("Entering method getMovieRatingByTicketsAmountInAscThatLess " +
                 "with long ticketsAmount = {}", ticketsAmount);
        Map<Movie, Long> filteredMoviesWithTicketsAmount
                = getMoviesWithTicketsAmountThatLess(
                getMoviesWithTicketsAmount(),
                ticketsAmount);

        LinkedHashMap<Movie, Long> rating = new LinkedHashMap<>();
        filteredMoviesWithTicketsAmount.entrySet()
                .stream()
                .sorted(comparingByValue())
                .forEachOrdered(x -> rating.put(x.getKey(), x.getValue()));
        log.info("Exiting method getMovieRatingByTicketsAmountInAscThatLess" +
                 " with result {}", rating);
        return rating;
    }

    private boolean isTicketForMovieAndDate(Ticket ticket, Movie movie,
                                            LocalDate showDate){
        return isTicketForDate(ticket, showDate)
                && isTicketForMovie(ticket, movie);
    }

    private boolean isDateIncludedInMonth(LocalDate date,
                                          int month,
                                          int year) {
        return date.getYear() == year && date.getMonth().getValue() == month;
    }

    private Map<Movie, Long> getMoviesWithTicketsAmount(){
        return getAll().stream()
                .collect(groupingBy(Ticket::getMovie, counting()));
    }

    private Map<Movie, Long> getMoviesWithTicketsAmountThatLess(
                        Map<Movie, Long> moviesWithTicketsAmount,
                        long ticketsAmount) {
        return moviesWithTicketsAmount.entrySet()
                .stream()
                .filter(a -> a.getValue().compareTo(ticketsAmount) < 0)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private boolean isTicketForToday(Ticket ticket) {
        return isTicketForDate(ticket, LocalDate.now());
    }

    private boolean isTicketForDate(Ticket ticket, LocalDate date) {
        return ticket.getDate().toLocalDate().equals(date);
    }

    private boolean isTicketForMovie(Ticket ticket, Movie movie) {
        return ticket.getMovie().equals(movie);
    }

    private void save(Ticket model) {
        localStorage.put(model.getId(),model);
    }

    private void checkIdForExisting(UUID id) {
        if(!localStorage.containsKey(id)) {
            NotFoundException exception
                    = new NotFoundException(Ticket.class.getName(), id);
            log.error("An exception occurred!", exception);
            throw exception;
        }
    }

    private Map<UUID, Ticket> getDataFromFileViaMap() {
        List<Ticket> dataList = fileUtility.read();
        return convertDataListToDataMap(dataList);
    }

    private Map<UUID, Ticket> convertDataListToDataMap(List<Ticket> dataList) {
        Map<UUID,Ticket> dataMap = new HashMap<>();
        dataList.forEach(genre -> dataMap.put(genre.getId(), genre));
        return dataMap;
    }
}
